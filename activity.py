name = "Dy"
age = 61
occupation = "student"
movie = "Avengers: Endgame"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for the movie {movie} is {rating}%.")

num1 = 75
num2 = 2
num3 = 76

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)